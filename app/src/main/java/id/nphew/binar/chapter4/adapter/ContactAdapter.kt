package id.nphew.binar.chapter4.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.nphew.binar.chapter4.R
import id.nphew.binar.chapter4.model.Contact

class ContactAdapter(val listContact: ArrayList<Contact>)
    : RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return ContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(listContact[position])
    }

    override fun getItemCount(): Int = listContact.size

    inner class ContactViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvName: TextView = view.findViewById(R.id.tv_nama)
        val tvPhone: TextView = view.findViewById(R.id.tv_phone)
        val ivContact : ImageView = view.findViewById(R.id.iv_contact)

        fun bind(item: Contact){
            tvName.text = item.nama
            tvPhone.text = item.phone
            ivContact.setImageResource(item.photo)
        }
    }
}