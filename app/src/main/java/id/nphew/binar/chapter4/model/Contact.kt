package id.nphew.binar.chapter4.model

data class Contact(
    val nama: String,
    val phone: String,
    val photo: Int
)
