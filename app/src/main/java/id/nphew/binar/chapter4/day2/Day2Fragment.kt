package id.nphew.binar.chapter4.day2

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import id.nphew.binar.chapter4.R
import id.nphew.binar.chapter4.databinding.FragmentDay2Binding


class Day2Fragment : Fragment() {
    private var _binding: FragmentDay2Binding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDay2Binding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAlertDialog()
        showAlertDialogWithAction()
        showAlertDialogCustom()
        showDialogFragment()
    }

    private fun showAlertDialog(){
        binding.btnDialog.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Disini Judul")
            dialog.setMessage("Disini pesan")
//            dialog.setCancelable(false)
            dialog.show()
        }
    }

    private fun showAlertDialogWithAction(){
        binding.btnDialogAction.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Judul")
            dialog.setMessage("Ini pesan")
            dialog.setPositiveButton("Positif"){ dialogInterface, angka ->
                createToast("Ini button positif").show()
            }
            dialog.setNegativeButton("Negatif"){ dialogInterface, _ ->

            }
            dialog.setNeutralButton("Netral"){ dialogInterface, _ ->

            }
//            dialog.setCancelable(false)
            dialog.show()
        }
    }
    private fun showAlertDialogCustom(){
        binding.btnDialogCustom.setOnClickListener {
            val customLayout = LayoutInflater.from(requireContext()).inflate(R.layout.layout_alert_dialog,null,false)
            val builder = AlertDialog.Builder(requireContext())
            val title = customLayout.findViewById<TextView>(R.id.tv_title)
            val buttonOne = customLayout.findViewById<Button>(R.id.btn_one)
            title.text = "Ini sudah diubah"
            buttonOne.text = "Dismiss"
            builder.setView(customLayout)

            val dialog = builder.create()
            buttonOne.setOnClickListener{
                dialog.dismiss()
            }
            binding.btnDialogCustom.setOnClickListener {
                dialog.show()
            }
        }
    }
    private fun showDialogFragment() {
        binding.btnDialogFragment.setOnClickListener {
            val dialogFragment = Day2DialogFragment()
            dialogFragment.show(requireActivity().supportFragmentManager, null)
            dialogFragment.isCancelable = false
        }
    }


    private fun  createToast(msg: String): Toast {
        return Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT)
    }
}