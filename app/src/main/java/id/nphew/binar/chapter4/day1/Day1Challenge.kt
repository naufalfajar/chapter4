package id.nphew.binar.chapter4.day1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar
import id.nphew.binar.chapter4.R
import id.nphew.binar.chapter4.databinding.FragmentDay1ChallengeBinding

class Day1Challenge : Fragment() {
    private var _binding: FragmentDay1ChallengeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDay1ChallengeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToast()
        createSnackbar()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun showToast() {
        binding.btnShownameToast.setOnClickListener {
            var etName = binding.etName.text.toString()
            etName = "Hello " + etName + ", welcome to my app!"
            Toast.makeText(requireContext(), etName, Toast.LENGTH_LONG).show()
        }
    }

    private fun createSnackbar() {
        binding.btnSnackbarAction.setOnClickListener {
            val snackbar = Snackbar.make(it, "Tes", Snackbar.LENGTH_INDEFINITE)
            snackbar.setAction("action1") {
                Toast.makeText(requireContext(), "Toast Action 1", Toast.LENGTH_LONG).show()
            }
                .addAction(R.layout.snackbar_extra_button, "action2") {
                    Toast.makeText(requireContext(), "Toast Action 2", Toast.LENGTH_LONG).show()
                }.show()
        }
    }

    fun Snackbar.addAction(
        @LayoutRes aLayoutId: Int,
        aLabel: String,
        aListener: View.OnClickListener?
    ): Snackbar {
        val button = LayoutInflater.from(view.context).inflate(aLayoutId, null) as Button
        view.findViewById<Button>(com.google.android.material.R.id.snackbar_action).let {
            button.layoutParams = it.layoutParams
            (button as? Button)?.setTextColor(it.textColors)
            (it.parent as? ViewGroup)?.addView(button)
        }
        button.text = aLabel
        button.setOnClickListener { this.dismiss(); aListener?.onClick(it) }
        return this;
    }
}