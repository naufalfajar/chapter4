package id.nphew.binar.chapter4.model

data class Person(
    val nama: String,
    val alamat: String
)
