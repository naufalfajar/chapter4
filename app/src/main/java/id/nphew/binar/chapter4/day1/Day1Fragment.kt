package id.nphew.binar.chapter4.day1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import com.google.android.material.snackbar.Snackbar
import id.nphew.binar.chapter4.R
import id.nphew.binar.chapter4.databinding.FragmentDay1Binding

class Day1Fragment : Fragment() {
    private var _binding: FragmentDay1Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDay1Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToast()
        showSnackbar()
        showSnackbarWithAction()
        binding.btnChallenge.setOnClickListener {
//           it.findNavController().navigate(R.id.action_day1Fragment_to_day1Challenge)
        }
    }

    private fun showToast(){
        binding.btnToast.setOnClickListener {
            Toast.makeText(requireContext(), "Button Toast telah ditekan!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun showSnackbar(){
        binding.btnSnackbar.setOnClickListener {
            Snackbar.make(it, "Button Snackbar telah ditekan!", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun showSnackbarWithAction(){
        binding.btnSnackbarAction.setOnClickListener {
            customColorSnackbar(it,"Button Snackbar Action telah ditekan!")
                .setAction(getString(R.string.txt_take_action)){
                    createToast("Take action ditekan!").show()
                }
                .show()
        }
    }

    private fun  createToast(msg: String): Toast{
        return Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT)
    }
    private fun  createSnackbar(it: View, msg: String): Snackbar{
        return Snackbar.make(it,msg,Snackbar.LENGTH_INDEFINITE)
    }
    private fun customColorSnackbar(it: View, msg: String): Snackbar{
        val snackbar = createSnackbar(it, msg)
        snackbar.setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.purple_500))
            .setActionTextColor(ContextCompat.getColor(requireContext(),R.color.black))
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
        return snackbar
    }

}